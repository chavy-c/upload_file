import { Uploader } from '../src/Uploader';

describe('Uploader test', () => {
  const uploader = new Uploader();

  it('should return true', async () => {
    const result = async () => {
      await uploader.savePagesAsCsv();
      return true;
    };

    expect(await result()).toBe(true);
  });

});