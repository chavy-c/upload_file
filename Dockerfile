FROM node:12.16.1
WORKDIR /app/
RUN npm install typescript -g --yes
# COPY ./package.json /app/
COPY . .
EXPOSE 3001
RUN npm install
# RUN tsc --build tsconfig.json

CMD ["npm", "run", "start:production"]