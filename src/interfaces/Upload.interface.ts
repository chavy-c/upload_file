export interface IElasticsearchConfig {
    host: string,
    // log: string
}

export interface IElasticsearchRecordResponse {
    id: number,
    first_name: string,
    last_name: string,
    email: string,
    gender: string,
    ip_address: string
}