
import {
    IElasticsearchConfig,
    IElasticsearchRecordResponse
} from '../interfaces';
import { fileFormater } from './FileFormater';
import elasticsearch from 'elasticsearch';
import { ES_IP, ES_PORT, LOG_LVL, DB_INDEX, DB_TYPE } from '../constants';

// module.exports = esClient;
const config: IElasticsearchConfig = {
    host: `elasticsearch:${ES_PORT}`,
    // log: `${LOG_LVL}`
};

class ElasticsearchClient {
    private esClient: elasticsearch.Client;
    constructor() {
        if (!this.esClient) {
            this.esClient = new elasticsearch.Client(config);
        }
    }

    /**
     * Create Index
     */
    public async createIndex(): Promise<void> {
        try {
            if (!await this.existsIndex()) {
                await this.esClient.indices.create({ index: DB_INDEX });
                console.log('created index');
            }
        } catch (e) {
            throw e;
        }
    }

    /**
     * Check if Index exists
     */
    private async existsIndex(): Promise<boolean> {
        return this.esClient.indices.exists({ index: DB_INDEX });
    }

    /**
     * Get All Records
     */
    private async getAllRecords(): Promise<IElasticsearchRecordResponse | any> {
        try {
            const userArray = [];
            const body: any = await this.esClient.search({
                index: DB_INDEX,
                type: DB_TYPE,
                scroll: '2m', // # Specify how long a consistent view of the index should be maintained for scrolled search
                size: 100,    // # Number of hits to return (default: 10)
                body: {
                    query: {
                        match_all: {}
                    }
                }
            });
            for (let data of body['hits']['hits']) {
                if (data) {
                    const userData = (data['_source'])
                    userArray.push(userData)
                }
            }
            return userArray;
        } catch (e) {
            throw e;
        }
    }

    /**
     * Proceed DB fulfill
     */
    private async importRecords(): Promise<void> {
        try {
            const records = await fileFormater.writeDBRecords();
            for (let record of records) {
                await this.esClient.index({
                    index: DB_INDEX,
                    type: DB_TYPE,
                    body: {
                        ...record
                    }
                });
            }
        } catch (e) {
            throw e;
        }
    }

    /**
     * exportData
     */
    public async exportData(): Promise<IElasticsearchRecordResponse | any> {
        await this.importRecords();
        return this.getAllRecords();
    }
}

export const client = new ElasticsearchClient();