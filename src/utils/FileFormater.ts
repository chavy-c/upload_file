import fs from 'fs';
import * as path from 'path';
import { IElasticsearchRecordResponse } from '../interfaces/Upload.interface';
import { PROJECT_PATH, DB_FILE_NAME, UPLOAD_FILE_NAME, DB_INDEX, DB_TYPE } from '../constants';

class FileFormater {
  private readonly fieldsEnglish = [
    'id', 'first_name', 'last_name', 'email', 'gender', 'ip_address'
  ];

  /**
   * import DB records to CSV format
   */
  public importToCSV(records: IElasticsearchRecordResponse | any): string {
    let resultCSVFile = '';
    resultCSVFile += this.fieldsEnglish + '\n';

    for (const record of records) {
      resultCSVFile += `${record.id}|`;
      resultCSVFile += `${record.first_name}|`;
      resultCSVFile += `${record.last_name}|`;
      resultCSVFile += `${record.email}|`;
      resultCSVFile += `${record.gender}|`;
      resultCSVFile += `${record.ip_address}`;
      resultCSVFile += '\n';
    }
    
    return resultCSVFile;
  }

  /**
   * Create the file with DB Records
   */
  public async writeCSVFile(formattedCSV: string): Promise<any> {
    return fs.writeFileSync(path.join(PROJECT_PATH, 'uploads', UPLOAD_FILE_NAME), formattedCSV);
  }

  /**
   * Read File and Import it to DB
   */
  public async writeDBRecords(): Promise<Object[]> {
    const filePath: string = path.join(PROJECT_PATH + '/imports' + DB_FILE_NAME);
    const file: string = fs.readFileSync(filePath, "utf8");
    return JSON.parse(file);
  }
}

export const fileFormater = new FileFormater();