import { IElasticsearchRecordResponse } from './interfaces';
import { client, fileFormater } from './utils';
import fs from 'fs';
import path from 'path';
import { PROJECT_PATH, UPLOAD_FILE_NAME } from './constants'

export class Uploader {

    /**
     * send CSV File
     */
    public async sendCSVFile(res: any): Promise<void> {
        // Fulfilling DB
        await client.createIndex();
        const records: IElasticsearchRecordResponse | unknown | any = await client.exportData();

        // Formating a file
        const formatedData: string = await fileFormater.importToCSV(records);
        // Sending a file
        await fileFormater.writeCSVFile(formatedData);

        res.writeHead(200, {
            'Content-Type': 'application/csv; charset=utf-8'
        });

        const readStream = fs.createReadStream(path.join(PROJECT_PATH, 'uploads', UPLOAD_FILE_NAME));
        // We replaced all the event handlers with a simple call to readStream.pipe()
        readStream.pipe(res);
    }
}
