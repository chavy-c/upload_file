export const PROJECT_PATH: string = `${process.cwd()}/src/utils/helpers`;
export const ES_IP: string = process.env.ES_IP || '127.0.0.1'; 
export const ES_PORT: number = 9200;
export const LOG_LVL: string = 'trace';
export const DB_FILE_NAME: string ='/dummy.json' ;
export const UPLOAD_FILE_NAME: string = '/upload.csv';
export const DB_INDEX = 'men';
export const DB_TYPE = 'users';