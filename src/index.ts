import http from 'http';
import { Uploader } from './Uploader';

const port = process.env.PORT || 8083;

http.createServer(async (request, response) => {
    const uploader: Uploader = new Uploader();
    return await uploader.sendCSVFile(response);
})
.listen(port);