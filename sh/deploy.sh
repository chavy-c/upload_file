#!/bin/sh

sudo su web
cd ~/fileUploader
git pull origin master --tags
npm install
pm2 restart ep.service.fileUploader
